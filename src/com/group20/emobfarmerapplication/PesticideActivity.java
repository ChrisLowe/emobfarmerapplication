/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		PesticideActivity.java
 * 
 * 			Calculates the cost of the pesticide spray.
 */


package com.group20.emobfarmerapplication;

import java.text.DecimalFormat;
import java.util.Dictionary;
import java.util.Hashtable;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class PesticideActivity extends Activity {

	
	private Button btnCalculate;
	private AlertDialog responseDialog;
	
	private Dictionary<String, Double> pesticidePrices;
	
	static final String LOG_TAG = "eMobFarmer.PesticideActivity";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesticide);
        
        pesticidePrices = new Hashtable<String, Double>();
        
        Handler handler = new Handler();
		final Runnable r = new Runnable() {
			 public void run() {
			 
				 Log.v(LOG_TAG, "Opening datastore for pesticide prices");
			        
			        Spinner spinnerPesticide = (Spinner) findViewById(R.id.spinner_pesticide);
			        DataStore ds = new DataStore(PesticideActivity.this);
			        Cursor c = ds.getPesticidePrices();
			        
			        try {
				        
				        c.moveToFirst();
				        Log.v(LOG_TAG, "Retreived " + c.getCount() + " values from the pesticide datastore");
				        
				        Log.v(LOG_TAG, "Retrieved: " + c.getInt(0) + ", " + c.getString(1) + ", " + c.getDouble(2));
				
				        // Retrieve the pesticide names
				        String[] pesticide = new String[c.getCount()];
				        for (int i = 0; i < c.getCount(); i++) {
				        	pesticide[i] = c.getString(1);
				        	pesticidePrices.put(c.getString(1), c.getDouble(2));
				        	Log.v(LOG_TAG, "Retreived value from pesticide datastore: " + c.getString(1) + ", " + c.getDouble(2));
				        	c.moveToNext();
				        }
				        
				        
				        //Set the spinner to the items in the string array
				        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PesticideActivity.this, android.R.layout.simple_spinner_item, pesticide);  
				        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
				        spinnerPesticide.setAdapter(adapter);  
				        
				        
				        
			        } catch (Exception e) {
			        	Log.e(LOG_TAG, "Unknown exception occured in pesticide activity: " + e.toString());
			        } finally {
			        	Log.v(LOG_TAG, "Closing cursor in pesticide activity");
			        	 c.close();
			        }
				 
				 
			 }
		};
		
		handler.post(r);
        
        

		 Spinner spinnerPesticide = (Spinner) findViewById(R.id.spinner_pesticide);
		 spinnerPesticide.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			 	public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			    	
			        TextView txtFertilizerPrice = (TextView) findViewById(R.id.label_pesticide_cost);
			        String selected = parentView.getItemAtPosition(position).toString();
			        Double cost = pesticidePrices.get(selected);
			        txtFertilizerPrice.setText("Price of " + selected + " is: $" + cost + " p/tonne");
			    }
			    
			    public void onNothingSelected(AdapterView<?> parentView) {
			        // your code here
			    }

			});
		
		
        
        //Prepare the response dialog
        responseDialog = new AlertDialog.Builder(this).create();
		responseDialog.setButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {

				  //Send a dialog message with the calculations and then return to the menu screen
				  Intent loginIntent = new Intent(PesticideActivity.this, MenuScreen.class);
				  startActivityForResult(loginIntent, 0);
				  responseDialog.hide();
			   }
			});
        
        
        btnCalculate = (Button) findViewById(R.id.button_pesticide_calculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				calculatePesticideCost();
				
			}
		});
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pesticide, menu);
        return true;
    }
    
    
    private void calculatePesticideCost() {
    	
    	
    			Spinner spinnerFertilizer = (Spinner) findViewById(R.id.spinner_pesticide);
    			String selected = spinnerFertilizer.getSelectedItem().toString();
    			EditText txtFieldSize = (EditText) findViewById(R.id.text_field);
    			EditText txtConcentration = (EditText) findViewById(R.id.text_concentration);
    			
    			double size = 0;
    			double concentration = 0;
    			double cost = 0;
    			
    			
    			// Validate and format the field size text
    			try {
    				size = Double.parseDouble(txtFieldSize.getText().toString());
    				
    				if (size == 0.0) {
    					throw new NumberFormatException();
    				}
    				
    				Log.v(LOG_TAG, "Field size validated: " + size + " ha");
    			} catch (NumberFormatException e) {
    				
    				Toast.makeText(getApplicationContext(), "Field size is not valid", Toast.LENGTH_SHORT).show();
    				Log.v(LOG_TAG, "Field size was not valid");
    				return;
    			}
    			
    			
    			// Validate and format the concentration text
    			try {
    				concentration = Double.parseDouble(txtConcentration.getText().toString());
    				if (concentration == 0.0) {
    					throw new NumberFormatException();
    				}
    				
    			} catch (NumberFormatException e) {
    				Toast.makeText(getApplicationContext(), "Concentration is not valid", Toast.LENGTH_SHORT).show();
    				Log.v(LOG_TAG, "Concentration was not valid");
    				return;
    			}
    			
    			// Validate the cost field (unknown edge condition - itemNotSelected?)
    			try {
    				cost = pesticidePrices.get(selected);
    			} catch (Exception e) {
    				Log.v(LOG_TAG, "Unknown exception occured - possibly item not selected: " + e.toString());
    				return;
    			}
    			
    			
    			Log.v(LOG_TAG, "Calculating pesticide prices for " + selected + " with values" + cost + " * " + size + " / " + concentration);
    			
    			
    			//Calculate cost
    			double totalCost = (cost * size) / concentration;
    			
    			// Final sanity check
    			if ((totalCost == Double.NaN) || (totalCost == 0.0)){
    				Toast.makeText(getApplicationContext(), "Could not calculate cost with these values.", Toast.LENGTH_SHORT).show();
    				Log.v(LOG_TAG, "totalCost sanity check failed: " + totalCost);
    				return;
    			}
    			
    		
    			// Prompt the results
    			String costString = "$" + new DecimalFormat("#.00").format(totalCost);
    	
    	

    	
    	responseDialog.setTitle(getResources().getString(R.string.string_pesticide_dialog_title));
		String str = getResources().getString(R.string.string_pesticide_dialog_message);
		str += costString;
		responseDialog.setMessage(str);
		responseDialog.show();
    
    }
    
}

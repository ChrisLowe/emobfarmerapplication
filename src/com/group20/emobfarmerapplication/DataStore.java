/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		DataStore.java
 * 
 * 			Handles the internal SQLite database on the phone.  When new data is retrieved from the MySQL database
 * 			it drops the tables, re-creates them and inserts the new data.
 */


package com.group20.emobfarmerapplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



public class DataStore extends SQLiteOpenHelper {

	final static String SQL_CREATE_WHEAT = "CREATE TABLE wheat (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, price DECIMAL NOT NULL);";
	final static String SQL_DROP_WHEAT = "DROP TABLE wheat;";
	final static String SQL_SELECT_WHEAT = "SELECT * FROM wheat;";
	
	final static String SQL_CREATE_FERTILIZER = "CREATE TABLE fertilizer (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, price DECIMAL NOT NULL);";
	final static String SQL_DROP_FERTILIZER = "DROP TABLE fertilizer;";
	final static String SQL_SELECT_FERTILIZER = "SELECT * FROM fertilizer;";
	
	final static String SQL_CREATE_PESTICIDE = "CREATE TABLE pesticide (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, price DECIMAL NOT NULL);";
	final static String SQL_DROP_PESTICIDE = "DROP TABLE pesticide;";
	final static String SQL_SELECT_PESTICIDE = "SELECT * FROM pesticide;";
	
	final static String SQL_CREATE_LAMB = "CREATE TABLE lamb (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, price DECIMAL NOT NULL);";
	final static String SQL_DROP_LAMB = "DROP TABLE lamb;";
	final static String SQL_SELECT_LAMB = "SELECT * FROM lamb;";
	
	
	final static String LOG_TAG = "eMobFarmer.DataStore";
	
	final static int DB_VERSION = 1;
	final static String DB_NAME = "emob.s3db";
	Context context;
	
	
	
	public DataStore(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		
		 
		try {
			database.execSQL(SQL_CREATE_WHEAT);
			Log.v(LOG_TAG, "onCreate created table wheat");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error in SQLite query: " + SQL_CREATE_WHEAT + " - " +e.toString());
		}
		
		try {
			database.execSQL(SQL_CREATE_FERTILIZER);
			Log.v(LOG_TAG, "onCreate created table fertilizer");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error in SQLite query: " + SQL_CREATE_FERTILIZER + " - " +e.toString());
		}
		
		try {
			database.execSQL(SQL_CREATE_LAMB);
			Log.v(LOG_TAG, "onCreate created table lamb");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error in SQLite query: " + SQL_CREATE_LAMB + " - " +e.toString());
		}
		
		try {
			database.execSQL(SQL_CREATE_PESTICIDE);
			Log.v(LOG_TAG, "onCreate created table pesticide");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error in SQLite query: " + SQL_CREATE_PESTICIDE + " - " +e.toString());
		}
		
	}
	
	
	public boolean isDatabasePopulated() {
		
		Cursor c = this.getReadableDatabase().rawQuery(SQL_SELECT_WHEAT, null);
		if (c.getCount() == 0) {
			return false;
		}
		
		return true;
	}

	
	public void updateWheatPrices() {
		
		// Query the PHP script for the wheat prices
		DatabaseAdapter db = new DatabaseAdapter();
		JSONArray jArray = db.queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getWheatPrices.php", null);

			
		// Wipe the existing wheat table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_DROP_WHEAT);
			sdb.close();
			Log.v(LOG_TAG, "Dropped wheat table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error deleting existing table " + SQL_DROP_WHEAT + " - " +e.toString());
		}
	    
	    // Create a fresh wheat table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_CREATE_WHEAT);
			sdb.close();
			Log.v(LOG_TAG, "Created wheat table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error creating table " + SQL_CREATE_WHEAT + " - " +e.toString());
		}
	    
	    // Insert the new values into the database
	    StringBuffer sqlInsertWheat = new StringBuffer();
	    try {
	    	for(int i=0;i<jArray.length();i++){

	    		// Create the SQL Insert statement with the JSON data
            	JSONObject json_data = jArray.getJSONObject(i);
            	sqlInsertWheat.append("INSERT INTO wheat VALUES (");
            	sqlInsertWheat.append(json_data.getInt("ID") + ", ");
            	sqlInsertWheat.append("'" + json_data.getString("name") + "', ");
            	sqlInsertWheat.append(json_data.getDouble("cost") + ");");
            	
            	//Update the SQLite database
        	    try {
        	    	SQLiteDatabase sdb = this.getWritableDatabase();
        			sdb.execSQL(sqlInsertWheat.toString());
        			sdb.close();
        			Log.v(LOG_TAG, "Executing INSERT into wheat table: " + sqlInsertWheat.toString());
        		} catch (SQLException e) {
        			Log.e(LOG_TAG, "Error in SQLite insert query " + sqlInsertWheat.toString() + " - " +e.toString());
        		}
        	    
        	    // Clear the StringBuffer to prepare the next insert statement
        	    sqlInsertWheat.delete(0, sqlInsertWheat.length());
            	
	    	}

	  	   
	    } catch (JSONException e) {
	    	Log.e("log_tag", "Error constructing SQLite insert statement: " + sqlInsertWheat.toString() + " - " +e.toString());
	    }
	
	}
	
	
	
	

	public void updateFertilizerPrices() {
		
		// Query the PHP script for the fertilizer prices
		DatabaseAdapter db = new DatabaseAdapter();
		JSONArray jArray = db.queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getFertilizerPrices.php", null);

			
		// Wipe the existing fertilizer table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_DROP_FERTILIZER);
			sdb.close();
			Log.v(LOG_TAG, "Dropped fertilizer table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error deleting existing table " + SQL_DROP_FERTILIZER + " - " +e.toString());
		}
	    
	    // Create a fresh wheat table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_CREATE_FERTILIZER);
			sdb.close();
			Log.v(LOG_TAG, "Created fertilizer table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error creating table " + SQL_CREATE_FERTILIZER + " - " +e.toString());
		}
	    
	    // Insert the new values into the database
	    StringBuffer sqlInsertFertilizer = new StringBuffer();
	    try {
	    	for(int i=0;i<jArray.length();i++){

	    		// Create the SQL Insert statement with the JSON data
            	JSONObject json_data = jArray.getJSONObject(i);
            	sqlInsertFertilizer.append("INSERT INTO fertilizer VALUES (");
            	sqlInsertFertilizer.append(json_data.getInt("id") + ", ");
            	sqlInsertFertilizer.append("'" + json_data.getString("name") + "', ");
            	sqlInsertFertilizer.append(json_data.getDouble("price") + ");");
            	
            	//Update the SQLite database
        	    try {
        	    	SQLiteDatabase sdb = this.getWritableDatabase();
        			sdb.execSQL(sqlInsertFertilizer.toString());
        			sdb.close();
        			Log.v(LOG_TAG, "Executing INSERT into fertilizer table: " + sqlInsertFertilizer.toString());
        		} catch (SQLException e) {
        			Log.e(LOG_TAG, "Error in SQLite insert query " + sqlInsertFertilizer.toString() + " - " +e.toString());
        		}
        	    
        	    // Clear the StringBuffer to prepare the next insert statement
        	    sqlInsertFertilizer.delete(0, sqlInsertFertilizer.length());
            	
	    	}
	    	
	  	   
	    } catch (JSONException e) {
	    	Log.e("log_tag", "Error constructing SQLite insert statement: " + sqlInsertFertilizer.toString() + " - " +e.toString());
	    }

	
	}
	
	
	
	
	
	

	public void updatePesticidePrices() {
		
		// Query the PHP script for the fertilizer prices
		DatabaseAdapter db = new DatabaseAdapter();
		JSONArray jArray = db.queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getPesticidePrices.php", null);

			
		// Wipe the existing fertilizer table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_DROP_PESTICIDE);
			sdb.close();
			Log.v(LOG_TAG, "Dropped pesticide table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error deleting existing table " + SQL_DROP_PESTICIDE + " - " +e.toString());
		}
	    
	    // Create a fresh wheat table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_CREATE_PESTICIDE);
			sdb.close();
			Log.v(LOG_TAG, "Created pesticide table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error creating table " + SQL_CREATE_PESTICIDE + " - " +e.toString());
		}
	    
	    // Insert the new values into the database
	    StringBuffer sqlInsertPesticide = new StringBuffer();
	    try {
	    	for(int i=0;i<jArray.length();i++){

	    		// Create the SQL Insert statement with the JSON data
            	JSONObject json_data = jArray.getJSONObject(i);
            	sqlInsertPesticide.append("INSERT INTO pesticide VALUES (");
            	sqlInsertPesticide.append(json_data.getInt("id") + ", ");
            	sqlInsertPesticide.append("'" + json_data.getString("name") + "', ");
            	sqlInsertPesticide.append(json_data.getDouble("price") + ");");
            	
            	//Update the SQLite database
        	    try {
        	    	SQLiteDatabase sdb = this.getWritableDatabase();
        			sdb.execSQL(sqlInsertPesticide.toString());
        			sdb.close();
        			Log.v(LOG_TAG, "Executing INSERT into pesticide table: " + sqlInsertPesticide.toString());
        		} catch (SQLException e) {
        			Log.e(LOG_TAG, "Error in SQLite pesticide query " + sqlInsertPesticide.toString() + " - " +e.toString());
        		}
        	    
        	    // Clear the StringBuffer to prepare the next insert statement
        	    sqlInsertPesticide.delete(0, sqlInsertPesticide.length());
            	
	    	}
	    	
	  	   
	    } catch (JSONException e) {
	    	Log.e("log_tag", "Error constructing SQLite insert statement: " + sqlInsertPesticide.toString() + " - " +e.toString());
	    }

	
	}
	

	
	


	public void updateLambPrices() {
		
		// Query the PHP script for the fertilizer prices
		DatabaseAdapter db = new DatabaseAdapter();
		JSONArray jArray = db.queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getLambPrices.php", null);

			
		// Wipe the existing fertilizer table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_DROP_LAMB);
			sdb.close();
			Log.v(LOG_TAG, "Dropped lamb table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error deleting existing table " + SQL_DROP_LAMB + " - " +e.toString());
		}
	    
	    // Create a fresh wheat table
	    try {
	    	SQLiteDatabase sdb = this.getWritableDatabase();
			sdb.execSQL(SQL_CREATE_LAMB);
			sdb.close();
			Log.v(LOG_TAG, "Created lamb table in SQLite database");
		} catch (SQLException e) {
			Log.e(LOG_TAG, "Error creating table " + SQL_CREATE_LAMB + " - " +e.toString());
		}
	    
	    // Insert the new values into the database
	    StringBuffer sqlInsertLamb = new StringBuffer();
	    try {
	    	for(int i=0;i<jArray.length();i++){

	    		// Create the SQL Insert statement with the JSON data
            	JSONObject json_data = jArray.getJSONObject(i);
            	sqlInsertLamb.append("INSERT INTO lamb VALUES (");
            	sqlInsertLamb.append(json_data.getInt("id") + ", ");
            	sqlInsertLamb.append("'" + json_data.getString("name") + "', ");
            	sqlInsertLamb.append(json_data.getDouble("price") + ");");
            	
            	//Update the SQLite database
        	    try {
        	    	SQLiteDatabase sdb = this.getWritableDatabase();
        			sdb.execSQL(sqlInsertLamb.toString());
        			sdb.close();
        			Log.v(LOG_TAG, "Executing INSERT into lamb table: " + sqlInsertLamb.toString());
        		} catch (SQLException e) {
        			Log.e(LOG_TAG, "Error in SQLite lamb query " + sqlInsertLamb.toString() + " - " +e.toString());
        		}
        	    
        	    // Clear the StringBuffer to prepare the next insert statement
        	    sqlInsertLamb.delete(0, sqlInsertLamb.length());
            	
	    	}
	    	
	  	   
	    } catch (JSONException e) {
	    	Log.e("log_tag", "Error constructing SQLite insert statement: " + sqlInsertLamb.toString() + " - " +e.toString());
	    }

	
	}
	
	
	public Cursor getLambPrices() {
		return this.getReadableDatabase().rawQuery(SQL_SELECT_LAMB, null);
	}
	
	
	public Cursor getWheatPrices() {
		return this.getReadableDatabase().rawQuery(SQL_SELECT_WHEAT, null);
	}
	
	public Cursor getFertilizerPrices() {
		return this.getReadableDatabase().rawQuery(SQL_SELECT_FERTILIZER, null);
	}
	
	public Cursor getPesticidePrices() {
		return this.getReadableDatabase().rawQuery(SQL_SELECT_PESTICIDE, null);
	}
	
	
	
	

	
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	

	
	
}

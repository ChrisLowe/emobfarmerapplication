/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		MenuScreen.java
 * 
 * 			The main menu activity.
 */


package com.group20.emobfarmerapplication;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MenuScreen extends Activity {

	
	private Button btnFertilizer;
	private Button btnPesticide;
	private Button btnMarket;
	private Button btnMap;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);
        
        btnFertilizer = (Button) findViewById(R.id.button_fertilizer);
        btnFertilizer.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent fertilizerIntent = new Intent(v.getContext(), FertilizerActivity.class);
				startActivityForResult(fertilizerIntent, 0);
				
			}
		});
        
        
        btnPesticide = (Button) findViewById(R.id.button_pesticide);
        btnPesticide.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent pesticideIntent = new Intent(v.getContext(), PesticideActivity.class);
				startActivityForResult(pesticideIntent, 0);
			}
		});
        
        btnMarket = (Button) findViewById(R.id.button_market);
        btnMarket.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent marketIntent = new Intent(v.getContext(), MarketActivity.class);
				startActivityForResult(marketIntent, 0);
			}
		});
        
        btnMap = (Button) findViewById(R.id.button_map);
        btnMap.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent mapIntent = new Intent(v.getContext(), PropertyMapActivity.class);
				startActivityForResult(mapIntent, 0);
				
			}
		});
        
   
        
      
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu_screen, menu);
        return true;
    }
}

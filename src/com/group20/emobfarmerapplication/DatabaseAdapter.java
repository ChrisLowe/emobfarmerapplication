/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		DatabaseAdapter.java
 * 
 * 			Connects, posts requests and parses the JSON results from the PHP files
 * 			which send requests to the MySQL database.
 */



package com.group20.emobfarmerapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class DatabaseAdapter {
	
	private static String LOG_TAG = "eMobFarmer.DatabaseAdapter";
	
	
	public DatabaseAdapter() {
		
	}
	
	
	
	/**
	 * Validates that the username is not currently in use
	 * 
	 * @param username
	 * @return true if the username is already in use
	 */
	public boolean isUsernameUsed(String username) {
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("username", username));

	    Log.v(LOG_TAG, "Validating username: " + username);
	    
	    try {
	    	JSONArray jarray = queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getUser.php", nameValuePairs);
			
	    	
	    	if (jarray.isNull(0)) {
	    		Log.v(LOG_TAG, "Username: " + username + " does not exist");
	    		return false;
	    	} else {
	    		Log.v(LOG_TAG, "Username: " + username + " already exists");
	    		return true;
	    	}
	    } catch (Exception e) {
	    	Log.v(LOG_TAG, "Username: " + username + " does not exist");
	    	return false;
	    }
	    	
	}
	
	
	/**
	 * Checks if the user name and password match in the database
	 * 
	 * @param username
	 * @param password
	 * @return true if the user name/password is correct
	 */
	public boolean isValidUsernamePassword(String username, String password) {
		
		if ((username.length() == 0) || (password.length() == 0)) {
			return false;
		}
		
		
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	    nameValuePairs.add(new BasicNameValuePair("username", username));
	    
	    try {
	    	JSONArray jarray = queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/getUser.php", nameValuePairs);
	    	JSONObject jdata = jarray.getJSONObject(0);
			
			if (jdata.getString("password").equals(password)) {
				Log.v(LOG_TAG, "User " + jdata.getString("name") + " logged in");
				return true;
			} else {
				// Invalid password
				Log.v(LOG_TAG, "Invalid login details: " + username + ", " + password);
				return false;
			}
	    
	    } catch (JSONException e) {
	    	// Invalid user name
	    	Log.v(LOG_TAG, "Invalid login details: " + username + ", " + password);
			return false;
	    }
				
	}
	
	
	/**
	 * This executes an online PHP script and returns the result in a JSONArray.
	 * If an error occurs or there is no result, null is returned the error is logged
	 * 
	 * 
	 * @param address The full http address of the PHP script to be executes
	 * @param postParameters The parameters to be passed into the PHP script or null
	 * @return  The results of the query or null if an error occured
	 */
	public JSONArray queryPhpForSqlResult(String address, ArrayList<NameValuePair> postParameters) {
		
		
	    // Create a HTTP post to the PHP file on the server to execute an SQL Query
	    try {
	    		Log.v(LOG_TAG, "Executing PHP query at address: " + address);
	            HttpClient httpclient = new DefaultHttpClient();
	            HttpPost httppost = new HttpPost(address);
	            
	            // Add the post parameters to the http response, if there are any
	            if ((postParameters != null) && (postParameters.size() > 0)) {
	            	Log.v(LOG_TAG, "Adding parameters: " + postParameters.toString());
	            	httppost.setEntity(new UrlEncodedFormEntity(postParameters));
	            }
	            
	            // Execute the HTTP query
	            HttpResponse response = httpclient.execute(httppost);
	            HttpEntity entity = response.getEntity();
	            InputStream is = entity.getContent();
	            
	            // Receive the response
	            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                    sb.append(line + "\n");
	            }
	            is.close();
	     
	            String result=sb.toString();
	            Log.v(LOG_TAG, "Retrieved result: " + result);
	            
	            return new JSONArray(result);
	            
	            
	    } catch (UnsupportedEncodingException e) {
	    	Log.e(LOG_TAG, "Error in encoding the response: "+e.toString());
	    	return null;
	            
	    } catch(ClientProtocolException e) {
	    	Log.e(LOG_TAG, "Error in http connection: "+e.toString());
	    	return null;
	    	 
	    } catch (IOException e) {
	    	Log.e(LOG_TAG, "Error in http stream: "+e.toString());
	    	return null;
	    	
	    } catch (JSONException e) {
            Log.e(LOG_TAG, "Error parsing data "+e.toString());
            return null;
	    }

	}

}

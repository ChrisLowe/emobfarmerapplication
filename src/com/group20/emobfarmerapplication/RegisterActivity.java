/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		RegisterActivity.java
 * 
 * 			Allows new users to register a username / password.  It does not:
 * 				- send an email for confirmation
 * 				- validate the email address
 */


package com.group20.emobfarmerapplication;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	
	private Button btnRegister;
	private AlertDialog responseDialog;

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        
        
        responseDialog = new AlertDialog.Builder(RegisterActivity.this).create();
		responseDialog.setTitle(getResources().getString(R.string.string_register_dialog_title));
		responseDialog.setMessage(getResources().getString(R.string.string_register_dialog_message));
		responseDialog.setButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {
				   //Send a dialog message indicating the registration is complete then return to the login activity
				   Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
				   startActivityForResult(loginIntent, 0);
			   }
		});
       
        
        btnRegister = (Button) findViewById(R.id.button_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			       EditText txtUsername = (EditText) findViewById(R.id.text_username);
			       EditText txtPassword = (EditText) findViewById(R.id.text_password);
			       EditText txtName = (EditText) findViewById(R.id.text_name);
			       EditText txtEmail = (EditText) findViewById(R.id.text_email);
				   
				   String username = txtUsername.getText().toString().trim();
				   String password = txtPassword.getText().toString().trim();
				   String name = txtName.getText().toString().trim();
				   String email = txtEmail.getText().toString().trim();
				   
				   
				   // Validate input fields
				   if (username.length() < 3) {
					   Toast.makeText(getApplicationContext(), "Usename field is invalid. Must be more than 3 characters.", Toast.LENGTH_SHORT).show();
				   } else if (password.length() < 3) {
					   Toast.makeText(getApplicationContext(), "Password field is invalid. Must be more than 3 characters.", Toast.LENGTH_SHORT).show();
				   } else if (name.length() < 3) {
					   Toast.makeText(getApplicationContext(), "Name is invalid. Must be more than 3 characters.", Toast.LENGTH_SHORT).show();
				   } else if (email.length() < 6) {
					   Toast.makeText(getApplicationContext(), "Email is invalid.", Toast.LENGTH_SHORT).show();
				   } else{
					   
					   
					   // Check that the username is not already in use
					   DatabaseAdapter db = new DatabaseAdapter();
					   
					   if (db.isUsernameUsed(username)) {
						   Toast.makeText(getApplicationContext(), "Username is already in use.", Toast.LENGTH_SHORT).show();
					   
					   } else {
						   
						   // Create the new user
						   ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						   
						   nameValuePairs.add(new BasicNameValuePair("name", name));
						   nameValuePairs.add(new BasicNameValuePair("username", username));
						   nameValuePairs.add(new BasicNameValuePair("password", password));
						   nameValuePairs.add(new BasicNameValuePair("email", email));
						   
						   db.queryPhpForSqlResult("http://www.lowware.com.au/eMobFarmer/createNewUser.php", nameValuePairs);
						   responseDialog.show();
						   
					   } 	   	
				   }
				}
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_register, menu);
        return true;
    }
}

/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		FertilizerActivity.java
 * 
 * 			The fertilizer spray cost calculation
 */

package com.group20.emobfarmerapplication;

import java.text.DecimalFormat;
import java.util.Dictionary;
import java.util.Hashtable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FertilizerActivity extends Activity {

	
	final static String LOG_TAG = "eMobFarmer.FertilizerActivity"; 
	
	private Button btnCalculate;
	private AlertDialog responseDialog;
	
	private Dictionary<String, Double> fertilizerPrices;
	
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fertilizer);
        
        // Name / Price dictionary
        fertilizerPrices = new Hashtable<String, Double>();
	
        Handler handler = new Handler();
		final Runnable r = new Runnable() {
			 public void run() {
				 
				 Spinner spinnerFertilizer = (Spinner) findViewById(R.id.spinner_fertilizer);
				 
				 Log.v(LOG_TAG, "Opening datastore for fertilizer prices");
			        DataStore ds = new DataStore(FertilizerActivity.this);
			        Cursor c = ds.getFertilizerPrices();
			        
			        try {
				        
				        c.moveToFirst();
				
				        // Retrieve the fertilizer names
				        String[] fertilizers = new String[c.getCount()];
				        for (int i = 0; i < c.getCount(); i++) {
				        	fertilizers[i] = c.getString(1);
				        	fertilizerPrices.put(c.getString(1), c.getDouble(2));
				        	c.moveToNext();
				        	Log.v(LOG_TAG, "Retrieved value from fertilizer table: " + fertilizers[i]);
				        }
				        
				        
				        //Set the spinner to the items in the string array
				        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FertilizerActivity.this, android.R.layout.simple_spinner_item, fertilizers);  
				        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
				        spinnerFertilizer.setAdapter(adapter);  
				        
				        //Set the default fertilizer price
				        c.moveToPosition(0);
				        
				        
			        } catch (Exception e) {
			        	Log.e(LOG_TAG, "Unknown exception occured in Fertilizer activity: " + e.toString());
			        } finally {
			        	Log.v(LOG_TAG, "Closing cursor in fertilizer activity");
			        	 c.close();
			        }
				 
				
			 }
		 };
			
		 handler.post(r);	

		 Spinner spinnerFertilizer = (Spinner) findViewById(R.id.spinner_fertilizer);
		 spinnerFertilizer.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			   
			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			    	
			        TextView txtFertilizerPrice = (TextView) findViewById(R.id.label_fertilizer_cost);
			        String selected = parentView.getItemAtPosition(position).toString();
			        Double cost = fertilizerPrices.get(selected);
			        txtFertilizerPrice.setText("Price of " + selected + " is: $" + cost + " p/tonne");
			    }
			    
			    public void onNothingSelected(AdapterView<?> parentView) {
			        // your code here
			    }

			});
      
        
        
        //Prepare the response dialog
        responseDialog = new AlertDialog.Builder(this).create();
		responseDialog.setButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {
				   
				  //Send a dialog message with the calculations and then return to the menu screen
				  Intent loginIntent = new Intent(FertilizerActivity.this, MenuScreen.class);
				  startActivityForResult(loginIntent, 0);
				  responseDialog.hide();
			   }
			});
        
        
        btnCalculate = (Button) findViewById(R.id.button_fertilizer_calculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				calculateFertilizerCost();
				

				
			}
		});
        
	}
	
	
	private void calculateFertilizerCost() {
		
		// 1. calculate cost of the spray
		//        - cost per tonne * size of field / concentration
		// 2. format it into a string   
		//		  -  123342.00  =>  $123,342.00
		
		Spinner spinnerFertilizer = (Spinner) findViewById(R.id.spinner_fertilizer);
		String selected = spinnerFertilizer.getSelectedItem().toString();
		EditText txtFieldSize = (EditText) findViewById(R.id.text_field);
		EditText txtConcentration = (EditText) findViewById(R.id.text_concentration);
		
		double size = 0;
		double concentration = 0;
		double cost = 0;
		
		
		// Validate and format the field size text
		try {
			size = Double.parseDouble(txtFieldSize.getText().toString());
			
			if (size == 0.0) {
				throw new NumberFormatException();
			}
			
			Log.v(LOG_TAG, "Field size validated: " + size + " ha");
		} catch (NumberFormatException e) {
			
			Toast.makeText(getApplicationContext(), "Field size is not valid", Toast.LENGTH_SHORT).show();
			Log.v(LOG_TAG, "Field size was not valid");
			return;
		}
		
		
		// Validate and format the concentration text
		try {
			concentration = Double.parseDouble(txtConcentration.getText().toString());
			if (concentration == 0.0) {
				throw new NumberFormatException();
			}
			
		} catch (NumberFormatException e) {
			Toast.makeText(getApplicationContext(), "Concentration is not valid", Toast.LENGTH_SHORT).show();
			Log.v(LOG_TAG, "Concentration was not valid");
			return;
		}
		
		// Validate the cost field (unknown edge condition - itemNotSelected?)
		try {
			cost = fertilizerPrices.get(selected);
		} catch (Exception e) {
			Log.v(LOG_TAG, "Unknown exception occured - possibly item not selected: " + e.toString());
			return;
		}
		
		
		Log.v(LOG_TAG, "Calculating fertilizer prices for " + selected + " with values" + cost + " * " + size + " / " + concentration);
		
		
		//Calculate cost
		double totalCost = (cost * size) / concentration;
		
		// Final sanity check
		if ((totalCost == Double.NaN) || (totalCost == 0.0)){
			Toast.makeText(getApplicationContext(), "Could not calculate cost with these values.", Toast.LENGTH_SHORT).show();
			Log.v(LOG_TAG, "totalCost sanity check failed: " + totalCost);
			return;
		}
		
	
		// Prompt the results
		String costString = "$" + new DecimalFormat("#.00").format(totalCost);
		responseDialog.setTitle(getResources().getString(R.string.string_fertilizer_dialog_title));
		String str = getResources().getString(R.string.string_fertilizer_dialog_message);
		str += costString;
		responseDialog.setMessage(str);
		responseDialog.show();
	}


}

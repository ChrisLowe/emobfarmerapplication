/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		ForgotPasswordActivity.java
 * 
 * 			The 'forgot password' activity.    TODO:  autosend an email to the address with a new password
 */

package com.group20.emobfarmerapplication;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class ForgotPasswordActivity extends Activity {

	
	private Button btnReset;
	AlertDialog responseDialog;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        
        responseDialog = new AlertDialog.Builder(this).create();
        
        btnReset = (Button) findViewById(R.id.button_reset);
        btnReset.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				
				responseDialog.setTitle(getResources().getString(R.string.string_forgot_reset_message_title));
				responseDialog.setMessage(getResources().getString(R.string.string_forgot_reset_message));
				responseDialog.setButton("OK", new DialogInterface.OnClickListener() {
					   public void onClick(DialogInterface dialog, int which) {
						   
						  //TODO:  implement password reset email
						   
						  //Send a dialog message indicating the password has been reset and then go back to the login activity
						  Intent loginIntent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
						  startActivityForResult(loginIntent, 0);
						  responseDialog.hide();
					   }
					});

				
				responseDialog.show();
				
			}
		});
        
        
        
    }

}

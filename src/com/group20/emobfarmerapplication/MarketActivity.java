/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		MarketActivity.java
 * 
 * 			The market prices activity.  It create the table of market prices dynamically
 */


package com.group20.emobfarmerapplication;

import java.text.DecimalFormat;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.Menu;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class MarketActivity extends Activity {
	
	final static String LOG_TAG = "eMobFarmer.MarketActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);
        
        // Create the table
        TableLayout table = (TableLayout) findViewById(R.id.price_table);
        TableRow tr_head = new TableRow(this);
        tr_head.setId(10);
        tr_head.setBackgroundColor(Color.GRAY);
        tr_head.setLayoutParams(new LayoutParams(
        		LayoutParams.FILL_PARENT,
        		LayoutParams.WRAP_CONTENT));
        
        // Create the table header
        TextView lblWheat = new TextView(this);
        lblWheat.setId(20);
        lblWheat.setText("Wheat");
        lblWheat.setTextColor(Color.WHITE);
        lblWheat.setPaintFlags(lblWheat.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        lblWheat.setPadding(5, 5, 5, 5);
        tr_head.addView(lblWheat);

        TextView lblPrice = new TextView(this);
        lblPrice.setId(21);
        lblPrice.setText("Price"); 
        lblPrice.setTextColor(Color.WHITE);
        lblPrice.setPaintFlags(lblPrice.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        lblPrice.setPadding(5, 5, 5, 5);
        tr_head.addView(lblPrice);
        
        // Add the header to the table
        table.addView(tr_head, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
        
        // Retrieve the wheat prices from the SQLite database
        
        
        DataStore ds = new DataStore(this);
        Cursor c = ds.getWheatPrices();
        
        try {
        	
  	        c.moveToFirst();
	        
	        int count = 0;
	       
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < c.getCount(); i++) {
	        	
	        	// Create the table cells
	        	TableRow tr = new TableRow(this);
	        	if(count%2!=0) {
	        		tr.setBackgroundColor(Color.GRAY);
	        	} else {
	        		tr.setBackgroundColor(Color.DKGRAY);
	        	}
	        	tr.setId(100+count);
	        	tr.setLayoutParams(new LayoutParams(
			        	LayoutParams.FILL_PARENT,
			        	LayoutParams.WRAP_CONTENT));
	
	        	TextView labelWheat = new TextView(this);
	        	labelWheat.setId(200+count); 
	        	labelWheat.setText(c.getString(1));
	        	labelWheat.setPadding(2, 0, 5, 0);
	        	labelWheat.setTextColor(Color.WHITE);
	        	tr.addView(labelWheat);
	        	
	        	TextView labelPrice = new TextView(this);
	        	labelPrice.setId(200+count);
	        	
	        	DecimalFormat df = new DecimalFormat("#.00");
	        	
	        	labelPrice.setText("$" + df.format(c.getDouble(2)));
	        	labelPrice.setTextColor(Color.WHITE);
	        	tr.addView(labelPrice);
	
	        	// Add the row to the table
	        	table.addView(tr, new TableLayout.LayoutParams(
	        		                    LayoutParams.FILL_PARENT,
	        		                    LayoutParams.WRAP_CONTENT));
	        	
	        	count++;
	        	
	        	sb.append(c.getInt(0) + ", ");
	        	sb.append(c.getString(1) + ", ");
	        	sb.append(c.getDouble(2) + "\n");
	        	c.moveToNext();
	        }
	        Log.v(LOG_TAG, sb.toString());
        
        } catch (Exception e) {
        	Log.e(LOG_TAG, "This is the shit i have to deal with in the market activity..." + e.toString());
        } finally {
        	 c.close();
        }
        
       
        	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_market, menu);
        return true;
    }
    
    
}

/*
 * 		eMobFarmer
 * 
 * 		Date: 2012-11-02
 * 		Developer: Christopher Lowe  (cjlowe@our.ecu.edu.au)
 * 		� 2012 All rights reserved
 *
 * 
 * 		LoginActivity.java
 * 
 * 			On the surface, this is just the user login page.  Functionally, it performs:
 * 				- the check for Internet connectivity
 * 				- validates the username/password
 * 				- updates the internal SQLite database from the online MySQL database and
 * 				- gives access to the register activity for first time users
 */

package com.group20.emobfarmerapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	
	private Button btnLogin;
	private Button btnOfflineMode;
	private EditText txtUsername;
	private EditText txtPassword;
	private TextView lblForgot;
	private TextView lblRegister;
	
	final static String LOG_TAG = "eMobFarmer.LoginActivity";
	

	
	
	private boolean isNetworkConnected() {
		  ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  return (cm.getActiveNetworkInfo() != null);
	}
	
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_login);
	        
	        
	        // Go straight to offline mode if there is no Internet access
	        if (!isNetworkConnected()) {
	        	
	        	// If there is no data in the SQLite database, prevent going into offline mode
	        	// This is an edge condition for when a user has never logged into the system
	        	if (!new DataStore(this).isDatabasePopulated()) {
	        		String msg = "Cannot enter offline mode.  You must log in at least once";
					Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	        	} else {
	        		String msg = "Entering offline mode";
					Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
					Intent mainMenuIntent = new Intent(getApplicationContext(), MenuScreen.class);
					startActivityForResult(mainMenuIntent, 0);
	        	}
	        }
	        
	        
	        txtUsername = (EditText) findViewById(R.id.text_username);
	        txtPassword = (EditText) findViewById(R.id.text_password);
	        
	        btnLogin = (Button) findViewById(R.id.button_login);
	        btnLogin.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					
					// Handle the online database is a separate thread so that the UI thread doesn't hang
					Handler handler = new Handler();
					final Runnable r = new Runnable() {
						 public void run() {
							 
							 	
							 DatabaseAdapter db = new DatabaseAdapter();
							 String username = txtUsername.getText().toString().trim();
							 String password = txtPassword.getText().toString().trim();
							 
							 // Validate user name/password
							 if (db.isValidUsernamePassword(username, password)) {
								 
								 // Update the SQLite database from the MySQL database
								 DataStore ds = new DataStore(LoginActivity.this);
								 
								 ds.updateWheatPrices();
								 ds.updateFertilizerPrices();
								 ds.updateLambPrices();
								 ds.updatePesticidePrices();
								 
								 Intent mainMenuIntent = new Intent(getApplicationContext(), MenuScreen.class);
								 startActivityForResult(mainMenuIntent, 0);
							} else {
								String msg = "Invalid username or password";
								Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
							}
					     }
					};
					handler.post(r);	
				}
			});
	        
	        btnOfflineMode = (Button) findViewById(R.id.button_offline);
	        btnOfflineMode.setOnClickListener(new View.OnClickListener() {
	        	public void onClick(View v) {
	        		if (!new DataStore(LoginActivity.this).isDatabasePopulated()) {
		        		String msg = "Cannot enter offline mode.  You must log in at least once";
						Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
		        	} else {
						Intent mainMenuIntent = new Intent(v.getContext(), MenuScreen.class);
						startActivityForResult(mainMenuIntent, 0);
		        	}
				}
	        });
	        
	        
	        lblForgot = (TextView) findViewById(R.id.label_forgot);
	        lblForgot.setPaintFlags(lblForgot.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
	        lblForgot.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent forgotPasswordIntent = new Intent(v.getContext(), ForgotPasswordActivity.class);
					startActivityForResult(forgotPasswordIntent, 0);
				}
			});
	        

	        lblRegister = (TextView) findViewById(R.id.label_register);
	        lblRegister.setPaintFlags(lblRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
	        lblRegister.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent registerIntent = new Intent(v.getContext(), RegisterActivity.class);
					startActivityForResult(registerIntent, 0);
				}
			});
	 }
	
}
